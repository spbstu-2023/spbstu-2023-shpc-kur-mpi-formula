# spbstu-2023-shpc-kur-mpi-formula
Библиотека с использовнием MPI для использования go-formula в многопоточном режиме.

## About

Author: Andrianov Artemii

Group: #5140904/30202

## Prerequisite
- [Go 1.21](https://tip.golang.org/doc/go1.21)
- [Docker](https://www.docker.com/)

## Build
Для сборки в качестве динамической SO библиотеки использовать:
```bash
    make docker-clib 
```

## SSA
Для создания файла с AST, SSA и прочимиgt шаги сборки использовать 
```bash
    make docker-gossa func=<name-of-func> file=<name-of-func-file>
```