//go:build exclude
// +build exclude

/*
	This package need for use code as golang library.
*/

package formula

/*
#cgo pkg-config: ompi
#include <mpi.h>
*/
import "C"
import (
	"fmt"
	"github.com/sandertv/go-formula/v2"
	log "github.com/sirupsen/logrus"
	"strings"
	"unsafe"
)

//export Init
func Init() (int, int) {
	C.MPI_Init(nil, nil)

	var size, rank C.int
	C.MPI_Comm_size(C.MPI_COMM_WORLD, &size)
	C.MPI_Comm_rank(C.MPI_COMM_WORLD, &rank)

	return int(size), int(rank)
}

//export Finalize
func Finalize() {
	C.MPI_Finalize()
}

// receiveData receiver data size and data from root proc.
//
//export receiveData
func receiveData(root int) []byte {
	size := 0
	C.MPI_Recv(unsafe.Pointer(&size), C.int(1), C.MPI_INT, C.int(root), 0, C.MPI_COMM_WORLD, C.MPI_STATUS_IGNORE)
	data := make([]byte, size)
	C.MPI_Recv(unsafe.Pointer(&data[0]), C.int(size), C.MPI_BYTE, C.int(root), 1, C.MPI_COMM_WORLD, C.MPI_STATUS_IGNORE)
	return data
}

// sendData send data size and data from root proc.
//
//export sendData
func sendData(root int, data []byte) {
	responseSize := len(data)
	C.MPI_Send(unsafe.Pointer(&responseSize), C.int(1), C.MPI_INT, C.int(root), 0, C.MPI_COMM_WORLD)
	C.MPI_Send(unsafe.Pointer(&data[0]), C.int(responseSize), C.MPI_BYTE, C.int(root), 1, C.MPI_COMM_WORLD)
}

// invalidFormulaResult is results with invalid formulas.
const invalidFormulaResult = "invalid formula!"

// calculateFormula return result of calculations and error
//
//export calculateFormula
func calculateFormula(form string) (float64, error) {
	f, err := formula.New(form)
	if err != nil {
		return 0, err
	}
	res, err := f.Eval()
	if err != nil {
		return 0, err
	}
	return res, nil
}

// ParallelCalculate get data from
//
//export ParallelCalculate
func ParallelCalculate(rank int, root int, suffix string) {
	data := receiveData(root)
	formulas := strings.Split(strings.TrimSuffix(string(data), suffix), suffix)

	results := make([]byte, 0)
	for _, form := range formulas {
		res, err := calculateFormula(form)
		if err != nil {
			log.Warningf("rank=%d: error while calculate %s: %s", int(rank), form, err.Error())
			results = append(results, []byte(fmt.Sprintf("%s=%s%s\n", form, invalidFormulaResult, suffix))...)
			continue
		}
		results = append(results, []byte(fmt.Sprintf("%s=%0.3f%s\n", strings.TrimSpace(form), res, suffix))...)
	}

	sendData(root, results)
}

// sendLinesToProcesses split lines and send data for all MPI processes.
//
//export sendLinesToProcesses
func sendLinesToProcesses(size int, lines []string) int {
	linesPerProcess := make([][]byte, size)
	iter := 1
	for _, line := range lines {
		linesPerProcess[iter] = append(linesPerProcess[iter], []byte(line)...)
		iter++
		if size == iter {
			iter = 1
		}
	}

	for i := 1; i < size; i++ {
		dataSize := len(linesPerProcess[i])
		if dataSize > 0 {
			C.MPI_Send(unsafe.Pointer(&dataSize), C.int(1), C.MPI_INT, C.int(i), 0, C.MPI_COMM_WORLD)
			C.MPI_Send(unsafe.Pointer(&linesPerProcess[i][0]), C.int(dataSize), C.MPI_BYTE, C.int(i), 1, C.MPI_COMM_WORLD)
		}
	}

	inUseProcCount := 0
	for _, line := range linesPerProcess {
		if len(line) > 0 {
			inUseProcCount++
		}
	}
	return inUseProcCount
}

// receiveDataFromProcesses receive data from all processes in use.
//
//export receiveDataFromProcesses
func receiveDataFromProcesses(isUseCount int) []string {
	results := make([]byte, 0)

	for i := 1; i <= isUseCount; i++ {
		dataSize := 0
		C.MPI_Recv(unsafe.Pointer(&dataSize), C.int(1), C.MPI_INT, C.int(i), 0, C.MPI_COMM_WORLD, C.MPI_STATUS_IGNORE)
		data := make([]byte, dataSize)
		C.MPI_Recv(unsafe.Pointer(&data[data[0]]), C.int(dataSize), C.MPI_BYTE, C.int(i), 1, C.MPI_COMM_WORLD, C.MPI_STATUS_IGNORE)
		results = append(results, data...)
	}

	splitted := strings.Split(string(results), "/n")

	return splitted
}

// HandleCalculations returns results of each formula in lines.
//
//export HandleCalculations
func HandleCalculations(size int, lines []string) []string {
	count := sendLinesToProcesses(size, lines)
	return receiveDataFromProcesses(count)
}
