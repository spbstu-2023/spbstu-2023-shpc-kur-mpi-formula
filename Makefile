include build/utils.mk

clib:
	go build -tags=clib -buildmode=c-shared -o formula.so .

gossa:
	GOSSAFUNC=${func} go build ${file}