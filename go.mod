module gitlab.com/spbstu-2023/spbstu-2023-shpc-kur-mpi-formula

go 1.21.4

require (
	github.com/sandertv/go-formula/v2 v2.0.0-alpha.7
	github.com/sirupsen/logrus v1.9.3
)

require (
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
)
